﻿#pragma once
#include <iostream>

#include "http_helpers.h"
#include "http_base.h"
#include "tcp_server.h"

struct ake_server_config : public base_server_config
{
  std::string hello_word;
  std::chrono::milliseconds send_timeout; // время, отведенное для отправки сообщения
  std::chrono::milliseconds read_timeout; // время, отведенное для приема сообщения
};

struct ake_client_config : public base_client_config
{
  std::string hello_word;
  std::chrono::milliseconds init_handshaking_timeout; // время, за которое должно осуществиться "согласование ключей" 
  std::chrono::milliseconds ping_timeout; // время, для обмена ping-pong сообщениями
  std::chrono::milliseconds ping_period; // период времени между двумя соседними ping-pong обменами
};

class ake_client : public http_client
{
public:
  ake_client(ba::io_context& ioc, std::unique_ptr<ake_client_config>&& config, std::string_view cna_name, bool set_silent) :
    http_client(ioc, std::move(config)), _cna_name{ cna_name }, _is_silent{ set_silent } {}
  void handle_error(bs::error_code& ec)
  {
    if(ec && !_is_silent)
      std::cerr << "AKE client error " << ec.value() << std::endl;
    disconnect();
  }
  void initialize(ba::yield_context yield, bs::error_code& ec) override
  {
    http_client::initialize(yield, ec);
    if (!ec)
      initial_handshaking(yield, ec);
  }
  void handle(ba::yield_context yield, bs::error_code& ec) override
  {
    if (ec) return handle_error(ec);
    auto ping = make_default_request(_cna_name, "ping");
    while (true)
    {
      _tcp_stream.expires_after(get_extended_config().ping_timeout);
      send(ping, yield, ec);
      if (ec) break;
      auto resp = recv(yield, ec);
      if (ec || resp.body() != "pong") break;
      if (!_is_silent)
        std::cout << "Client receive: " << resp.body() << std::endl;
      async_sleep(get_extended_config().ping_period, yield, ec);
      if (ec) break;
    }
    return handle_error(ec);
  }
  void finish(ba::yield_context yield, bs::error_code& ec) override
  {
    // тут чистим лишнее
    http_client::finish(yield, ec);
    // тут можно переподключиться
  }
private:
  void on_timeout()
  {
    if (!_is_silent)
      std::cout << "Client timeout\n";
    disconnect();
  }
  void initial_handshaking(ba::yield_context yield, bs::error_code& ec)
  {
    _tcp_stream.expires_after(get_extended_config().init_handshaking_timeout);
    do {
      auto req = make_default_request(_cna_name, get_extended_config().hello_word);
      send(req, yield, ec);
      if (ec) break;
      auto resp = recv(yield, ec);
      if (ec) break;
      if (!_is_silent)
        std::cout << "Client: initial handhaking response 1: " << resp.body() << std::endl;
      req = make_default_request(_cna_name, _cna_name);
      send(req, yield, ec);
      if (ec) break;
      resp = recv(yield, ec);
      if (ec) break;
      if (!_is_silent)
        std::cout << "Client: initial handhaking response 2: : " << resp.body() << std::endl;
    } while (false);
  }
  ake_client_config& get_extended_config()
  {
    return *(static_cast<ake_client_config*>(_config.get()));
  }
private:
  std::string _cna_name;
  bool _is_silent;
};

class ake_handler : public http_server_connection
{
public:
  ake_handler(tcp::socket&& s, ake_server_config config, std::string cna_name, bool set_silent) :
    http_server_connection(std::move(s)), _cna_name{ cna_name }, _config{ config }, _is_silent{ set_silent } {}

  void handle_error(bs::error_code& ec)
  {
    if (!_is_silent)
      std::cerr << "AKE server error " << ec.value() << std::endl;
    disconnect();
  }
  void initialize(ba::yield_context yield, bs::error_code& ec) override
  {
    http_server_connection::initialize(yield, ec);
    if (!ec)
      initial_handshaking(yield, ec);
  }
  void handle(ba::yield_context yield, bs::error_code& ec) override
  {
    if (ec) handle_error(ec);
    while (true)
    {
      auto req = recv(yield, ec);
      if (ec) break;
      if (!_is_silent)
        std::cout << "Server receive: " << req.body() << std::endl;
      if (req.method() == http::verb::post && req.body() == "ping")
      {
        auto resp = make_default_response("pong");
        send(resp, yield, ec, _config.send_timeout);
      }
    }
    return handle_error(ec);
  }

private:
  void initial_handshaking(ba::yield_context yield, bs::error_code& ec)
  {
    do {
      auto req = recv(yield, ec, _config.read_timeout);
      if (ec) break;
      if (!_is_silent)
        std::cout << "Server: initial handhaking request 1: " << req.body() << std::endl;
      auto resp = make_default_response(_config.hello_word);
      send(resp, yield, ec, _config.send_timeout);
      if (ec) break;
      req = recv(yield, ec, _config.read_timeout);
      if (ec) break;
      if (!_is_silent)
        std::cout << "Server: initial handhaking request 2: " << req.body() << std::endl;
      resp = make_default_response(_cna_name);
      send(resp, yield, ec, _config.send_timeout);
    } while (false);
  }

  void on_timeout()
  {
    if (!_is_silent)
      std::cout << "AKE server timeout!\n";
    disconnect();
  }
private:
  bool _is_silent;
  std::string _cna_name;
  ake_server_config _config;
};

class ake_server : public base_tcp_server
{
public:
  ake_server(boost::asio::io_context& ioc, std::unique_ptr<ake_server_config>&& config, std::string_view cna_name, bool set_silent) :
    base_tcp_server(ioc, std::move(config)), _cna_name{ cna_name }, _is_silent{ set_silent } {}
private:
  std::shared_ptr<async_tcp_handler> make_connection(tcp::socket&& s) override
  {
    if (!_is_silent)
      std::cout << "New AKE connection from " << s.remote_endpoint().address().to_string() << std::endl;

    return static_cast<std::shared_ptr<async_tcp_handler>>(std::make_shared<ake_handler>(std::move(s),
      get_extended_config(), _cna_name, _is_silent));
  }
  ake_server_config& get_extended_config()
  {
    return *(static_cast<ake_server_config*>(_config.get()));
  }
private:
  std::string _cna_name;
  bool _is_silent;
};
