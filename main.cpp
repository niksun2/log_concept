﻿#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <boost/asio/error.hpp>
#include <boost/asio.hpp>
#include <thread>
#include <sstream>
#include <iostream>

#include "log/init.h"
#include "ake_parody.h"

#define THREAD_COUNT 8
#define CLIENT_COUNT 5

std::unique_ptr<ake_server_config> make_server_config()
{
  auto server_config = std::make_unique<ake_server_config>();
  server_config->listen_ip = "0.0.0.0";
  server_config->listen_port = 12345u;
  server_config->hello_word = "Hello from server!";
  server_config->read_timeout = std::chrono::seconds(5);
  server_config->send_timeout = std::chrono::seconds(5);
  return server_config;
}

std::unique_ptr<ake_client_config> make_client_config(int client_number)
{
  auto client_config = std::make_unique<ake_client_config>();
  client_config->server_ip = "127.0.0.1";
  client_config->server_port = 12345u;
  std::ostringstream temp;
  temp << "Hello from client #" << client_number << "!";
  client_config->hello_word = temp.str();
  client_config->init_handshaking_timeout = std::chrono::seconds(10);
  client_config->ping_period = std::chrono::seconds(3);
  client_config->ping_timeout = std::chrono::seconds(10);
  return client_config;
}

class ioc_guard
{
  boost::asio::io_context& _ioc;
  boost::asio::io_context::work _work;
  std::list<std::shared_ptr<std::thread>> _threads;
public:
  ioc_guard(boost::asio::io_context& ioc) : _ioc{ ioc }, _work{ ioc } {}
  void run(int thread_count)
  {
    for (int i = 0; i < thread_count; i++)
      _threads.emplace_back(std::make_shared<std::thread>([this]() {_ioc.run(); }));
  }
  ~ioc_guard()
  {
    _ioc.stop();
    for (auto thread : _threads)
      if(thread->joinable())
        thread->join();
    _ioc.reset();
  }
};

int main()
{
  init_boost_log(boost::log::trivial::severity_level::trace, "logfile.txt");
  boost::asio::io_context ioc{ THREAD_COUNT };
  ioc_guard guard(ioc);
  guard.run(THREAD_COUNT);  
  ake_server server(ioc, make_server_config(), "CNA006667", true);
  server.start();
  std::list<std::shared_ptr<ake_client>> clis;
  for (int i = 0; i < CLIENT_COUNT; i++)
  {
    std::ostringstream temp;
    temp << "CNA00" << std::setfill('0') << std::setw(4) << i;
    clis.push_back(std::make_shared<ake_client>(ioc, make_client_config(i), temp.str(), true));
    clis.back()->connect();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  } 
  std::cout << "Press any key for stop server\n";
  getc(stdin);
  std::cout << server.get_connections_status().size() << " connections established" << std::endl;
  server.stop();
}
