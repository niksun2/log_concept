#include "tcp_server.h"

#pragma region connection_list
connection_list::connection_list(size_t optimal_capacity) :
  _optimal_capacity{ optimal_capacity }, _mutex{}, _connections{} {}
std::shared_ptr<async_tcp_handler> connection_list::append(const std::shared_ptr<async_tcp_handler>& connection)
{
  std::unique_lock<std::mutex> lock(_mutex);
  _connections.push_back(connection);
  if (_connections.size() > _optimal_capacity)
    clean();
  return connection;
}
size_t connection_list::size() const
{
  return _connections.size();
}
void connection_list::foreach(const std::function<bool(std::shared_ptr<async_tcp_handler>&)>& action)
{
  std::unique_lock<std::mutex> lock(_mutex);
  for (auto& connection : _connections)
    if (!action(connection))
      break;
}
void connection_list::foreach(const std::function<bool(const std::shared_ptr<const async_tcp_handler>&)>& action) const
{
  std::unique_lock<std::mutex> lock(_mutex);
  for (const auto& connection : _connections)
    if (!action(connection))
      break;
}
void connection_list::clean()
{
  std::unique_lock<std::mutex> lock(_mutex);
  auto connection = _connections.begin();
  while (connection != _connections.end())
  {
    if (!(*connection)->is_active())
      connection = _connections.erase(connection);
    else
      ++connection;
  }
  TLOG("io/connection_list", << "List cleaned.");
}
#pragma endregion connection_list

#pragma region base_tcp_server
base_tcp_server::base_tcp_server(boost::asio::io_context& ioc, std::unique_ptr<base_server_config>&& config) :
  _acceptor{ ioc }, _config{ std::move(config) }, _connections(OPTIMAL_MAX_CONNECTION_SIZE) {}

bool base_tcp_server::start()
{
  TLOG("io/base_tcp_server", << "Starting server");
  bs::error_code ec;
  do
  {
    auto listen_ip_address = ba::ip::make_address(_config->listen_ip, ec);
    if (ec) break;
    tcp::endpoint listen_endpoint{ listen_ip_address, _config->listen_port };
    _acceptor.open(listen_endpoint.protocol(), ec);
    if (ec) break;
    _acceptor.bind(listen_endpoint, ec);
    if (ec) break;
    _acceptor.listen(boost::asio::socket_base::max_listen_connections, ec);
    if (ec) break;
  } while (false);
  if (ec)
  {
    ELOG("io/base_tcp_server", << "Error on starting server  " << EXPAND_EC(ec));
    return false;
  }
  boost::asio::spawn(_acceptor.get_executor(), [this](ba::yield_context yield) {this->listening(yield); });
  TLOG("io/base_tcp_server", << "Server started");
  return true;
}

void base_tcp_server::stop()
{
  TLOG("io/base_tcp_server", << "Stopping server");
  if (_acceptor.is_open())
  {
    bs::error_code ec;
    _acceptor.close(ec);
    if (ec) ELOG("io/base_tcp_server", << "Error on closing acceptor  " << EXPAND_EC(ec));
  }
  disconnect_all();
  TLOG("io/base_tcp_server", << "Server stopped");
}

bool base_tcp_server::restart()
{
  stop();
  return start();
}

std::vector<std::pair<std::string, connection_status>> base_tcp_server::get_connections_status() const
{
  std::vector<std::pair<std::string, connection_status>> result;
  result.reserve(_connections.size());
  _connections.foreach([&result](const std::shared_ptr<const async_tcp_handler>& c) -> bool
    {
      bs::error_code ec;
      std::string client_name;
      do {
        auto client_address = c->get_remote_address(ec);
        if (ec) break;
        client_name = client_address.to_string(ec);
      } while (false);
      if (ec)
        client_name = "unknown";
      result.push_back(std::make_pair(client_name, c->get_status()));
      return true;
    });
  return result;
}

void base_tcp_server::disconnect_all()
{
  TLOG("io/base_tcp_server", << "Disconnecting all clients");
  _connections.foreach([](std::shared_ptr<async_tcp_handler>& c)  -> bool {c->disconnect(); return true; });
  TLOG("io/base_tcp_server", << "All clients disconnected");
}

std::shared_ptr<async_tcp_handler> base_tcp_server::lookup(std::string_view client_ip_address)
{
  TLOG("io/base_tcp_server", << "Searching client with ip address " << client_ip_address);
  bs::error_code ec;
  auto target_ip = ba::ip::make_address(client_ip_address, ec);
  if (ec)
  {
    ELOG("io/connection_list", << "Error on lookup " << EXPAND_EC(ec));
    return nullptr;
  }
  std::shared_ptr<async_tcp_handler> result;
  _connections.foreach([&target_ip, &result, &ec](std::shared_ptr<async_tcp_handler>& c)  -> bool {
    if (c->get_remote_address(ec) == target_ip)
    {
      result = c;
      return false;
    }
    return true;
    });
  return result;
}

void base_tcp_server::listening(boost::asio::yield_context yield)
{
  bs::error_code ec;
  for (;;)
  {
    tcp::socket s{ _acceptor.get_executor() };
    _acceptor.async_accept(s, yield[ec]);
    if (ec)
    {
      if (ec != ba::error::operation_aborted)
      {
        ELOG("io/base_tcp_server", << "Error on listening  " << EXPAND_EC(ec));
        _acceptor.close(ec);
        if (ec) ELOG("io/base_tcp_server", << "Error on closing acceptor  " << EXPAND_EC(ec));
      }
      break;
    }
    auto re = s.remote_endpoint(ec);
    if (!ec)
    {
      auto remote_ip = re.address().to_string(ec);
      if (!ec)
        ILOG("io/base_tcp_server", << "New connection from " << remote_ip << ":" << re.port());
    }
    auto new_connection = _connections.append(make_connection(std::move(s)));
    ba::spawn(_acceptor.get_executor(),
      [new_connection](ba::yield_context yield) {new_connection->main_coroutine(yield); });
  }
}

#pragma endregion base_tcp_server
