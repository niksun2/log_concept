﻿#include "async_tcp_handler.h"

#pragma region async_tcp_handler definition
async_tcp_handler::async_tcp_handler(tcp::socket&& socket) :
  _tcp_stream{ std::move(socket) }, _status{ connection_status::closed }, _connection_name_for_log{}
{
  update_connection_name();
}
void async_tcp_handler::main_coroutine(boost::asio::yield_context yield)
{
  bs::error_code ec;
  _status = connection_status::connecting;
  initialize(yield, ec);
  if (!ec)
  {
    _status = connection_status::establish;
    handle(yield, ec);
  }
  else
    ELOG(_connection_name_for_log, << "Error on initializing: " << EXPAND_EC(ec));
  finish(yield, ec);
}
void async_tcp_handler::disconnect()
{
  if (_status == connection_status::closing || _status == connection_status::closed)
    return;
  _status = connection_status::closing;
  close_socket();
  _status = connection_status::closed;
}
ba::ip::address async_tcp_handler::get_remote_address(bs::error_code& ec) const
{
  auto ep = _tcp_stream.socket().remote_endpoint(ec);
  if (ec)
    return ba::ip::address_v4::any();
  return ep.address();
}
void async_tcp_handler::close_socket()
{
  bs::error_code ec;
  if (_tcp_stream.socket().is_open())
  {
    _tcp_stream.socket().shutdown(tcp::socket::shutdown_both, ec);
    if (ec) ELOG(_connection_name_for_log, << "Error on shuting down socket: " << EXPAND_EC(ec));
    _tcp_stream.close();
    if (ec) ELOG(_connection_name_for_log, << "Error on closing socket: " << EXPAND_EC(ec));
  }
  TLOG(_connection_name_for_log, << "Socket closed");
}
bs::error_code async_tcp_handler::update_connection_name()
{
  bs::error_code ec;
  do{
    auto le = _tcp_stream.socket().local_endpoint(ec);
    if (ec) break;
    auto local_ip = le.address().to_string(ec);
    if (ec) break;
    auto re = _tcp_stream.socket().remote_endpoint(ec);
    if (ec) break;
    auto remote_ip = re.address().to_string(ec);
    if (ec) break;
    std::stringstream temp_stream;
    temp_stream << "io/connection[ " << local_ip << ":" << le.port() << "->" << remote_ip << ":" << re.port() << " ]";
    _connection_name_for_log = temp_stream.str();
  } while (false);
  if (ec)
    _connection_name_for_log = "io/connection[ disconnected ]";
  return ec;
}
void async_tcp_handler::async_sleep(const std::chrono::milliseconds& t, boost::asio::yield_context yield, bs::error_code & ec)
{
  ba::steady_timer local_timer(_tcp_stream.get_executor(), t);
  TLOG(_connection_name_for_log, << "Start waiting " << t.count() << " ms");
  local_timer.async_wait(yield[ec]);
  TLOG(_connection_name_for_log, << "Stop waiting");
}
template<class AsyncWriteStream, bool isRequest>
http::message<isRequest, http::string_body> async_tcp_handler::receive_message(AsyncWriteStream& stream, boost::asio::yield_context yield,
  bs::error_code& ec, const std::chrono::milliseconds& timeout)
{
  http::message<isRequest, http::string_body> message;
  boost::beast::flat_buffer buffer;

  if (timeout.count() > 0)
    _tcp_stream.expires_after(timeout);
  auto size = http::async_read(stream, buffer, message, yield[ec]);
  if (timeout.count() > 0)
    _tcp_stream.expires_never();

  if (!ec)
    ILOG(_connection_name_for_log, << "Message recieved, with length " << size);
  else if (ec != ba::error::operation_aborted)
    ELOG(_connection_name_for_log, << "Recieve message error: " << EXPAND_EC(ec));
  return message;
}
template<class AsyncWriteStream, bool isRequest>
void async_tcp_handler::send_message(AsyncWriteStream& stream, const http::message<isRequest, http::string_body>& message, boost::asio::yield_context yield,
  bs::error_code& ec, const std::chrono::milliseconds& timeout)
{
  if (timeout.count() > 0)
    _tcp_stream.expires_after(timeout);
  auto size = http::async_write(stream, message, yield[ec]);
  if (timeout.count() > 0)
    _tcp_stream.expires_never();

  if (!ec)
    ILOG(_connection_name_for_log, << "Message sent, with length " << size);
  else if (ec != ba::error::operation_aborted)
    ELOG(_connection_name_for_log, << "Send message error: " << EXPAND_EC(ec));
}
#pragma endregion 


template void async_tcp_handler::send_message<boost::beast::tcp_stream, false>(boost::beast::tcp_stream&, const http::response<http::string_body>&,
  boost::asio::yield_context, bs::error_code&, const std::chrono::milliseconds&);
template void async_tcp_handler::send_message<boost::beast::tcp_stream, true>(boost::beast::tcp_stream&, const http::request<http::string_body>&,
  boost::asio::yield_context, bs::error_code&, const std::chrono::milliseconds&);

template http::response<http::string_body> async_tcp_handler::receive_message<boost::beast::tcp_stream, false>(boost::beast::tcp_stream&,
  boost::asio::yield_context yield, bs::error_code& ec, const std::chrono::milliseconds&);
template http::request<http::string_body> async_tcp_handler::receive_message<boost::beast::tcp_stream, true>(boost::beast::tcp_stream&,
  boost::asio::yield_context yield, bs::error_code& ec, const std::chrono::milliseconds&);
