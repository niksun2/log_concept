#include "async_client.h"

#pragma region async_client definition
template<class async_protocol_handler, typename client_config>
async_client<async_protocol_handler, client_config>::async_client(boost::asio::io_context& ioc, std::unique_ptr<client_config>&& config) :
  async_protocol_handler(tcp::socket{ ioc }), _config{ std::move(config) } {}
template<class async_protocol_handler, typename client_config>
bool async_client<async_protocol_handler, client_config>::connect()
{
  if (_status != connection_status::closed)
    return false;
  bs::error_code ec;
  do {
    if (!_config->local_ip.empty())
    {
      auto local_ip_address = ba::ip::make_address(_config->local_ip, ec);
      if (ec) break;
      _tcp_stream.socket().bind({ local_ip_address, 0 }, ec);
      if (ec) break;
    }
    auto server_ip_address = ba::ip::make_address(_config->server_ip, ec);
    if (ec) break;
    tcp::endpoint remote_endpoint{ server_ip_address, _config->server_port };
    _status = connection_status::connecting;
    auto inst = shared_from_this();
    _tcp_stream.async_connect(remote_endpoint, [inst](bs::error_code ec) {inst->on_connect(ec); });
  } while (false);
  if (ec)
  {
    ELOG("io/async_client", << "Connection error " << EXPAND_EC(ec));
    return false;
  }
  return true;
}
template<class async_protocol_handler, typename client_config>
void async_client<async_protocol_handler, client_config>::on_connect(bs::error_code ec)
{
  if (ec)
  {
    ELOG("io/async_client", << "Connection error " << EXPAND_EC(ec));
    _status = connection_status::closed;
    return;
  }
  update_connection_name();
  ILOG(_connection_name_for_log, << "Client connected");
  auto inst = shared_from_this();
  ba::spawn(_tcp_stream.get_executor(),
    [inst](ba::yield_context yield) {inst->main_coroutine(yield); });
}
template<class async_protocol_handler, typename client_config>
std::shared_ptr<async_client<async_protocol_handler, client_config>> async_client<async_protocol_handler, client_config>::shared_from_this()
{
  return std::static_pointer_cast<async_client<async_protocol_handler, client_config>>(async_tcp_handler::shared_from_this());
}
#pragma endregion

template async_client<async_tcp_handler, base_client_config>;