﻿#pragma once
#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

#include <atomic>

#include "common.h"
#include "log.h"

namespace ba = boost::asio;
namespace bs = boost::system;
using tcp = boost::asio::ip::tcp;
namespace http = boost::beast::http;

class async_tcp_handler :public std::enable_shared_from_this<async_tcp_handler>
{
public:
  async_tcp_handler(tcp::socket&& socket);
  bool is_active() const { return _status != connection_status::closed; }
  connection_status get_status() const { return _status; }
  ba::ip::address get_remote_address(bs::error_code& ec) const;
  void main_coroutine(boost::asio::yield_context yield);
  virtual void disconnect();
protected:
  virtual void initialize(boost::asio::yield_context yield, bs::error_code& ec) {}; // not pure virtual method for protocols without init
  virtual void finish(boost::asio::yield_context yield, bs::error_code& ec) { disconnect(); }
  virtual void handle(boost::asio::yield_context yield, bs::error_code& ec) = 0;
  void close_socket();
  bs::error_code update_connection_name();
  template<class AsyncWriteStream, bool isRequest>
  http::message<isRequest, http::string_body> receive_message(AsyncWriteStream& stream, boost::asio::yield_context yield,
    bs::error_code& ec, const std::chrono::milliseconds& timeout = std::chrono::milliseconds::zero());
  template<class AsyncWriteStream, bool isRequest>
  void send_message(AsyncWriteStream& stream, const http::message<isRequest, http::string_body>& message, boost::asio::yield_context yield,
    bs::error_code& ec, const std::chrono::milliseconds& timeout = std::chrono::milliseconds::zero());
  void async_sleep(const std::chrono::milliseconds& t, ba::yield_context yield, bs::error_code& ec);
protected:
  boost::beast::tcp_stream _tcp_stream;
  connection_status _status;
  std::string _connection_name_for_log;
};
