﻿#pragma once

struct base_server_config // for tcp_server
{
  std::string listen_ip;
  uint16_t listen_port;
};


struct base_client_config // for http_client
{
  std::string local_ip;
  std::string server_ip;
  uint16_t server_port;
};