﻿#pragma once
#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

#include "async_tcp_handler.h"
#include "async_client.h"
#include "common.h"
#include "config.h"

namespace http = boost::beast::http;
namespace ba = boost::asio;
namespace bs = boost::system;
using tcp = boost::asio::ip::tcp;

class http_server_connection : public async_tcp_handler
{
public:
  http_server_connection(tcp::socket&& s);
protected:
  http::request<http::string_body> recv(boost::asio::yield_context yield,
    bs::error_code& ec, const std::chrono::milliseconds& timeout = std::chrono::milliseconds::zero());
  void send(const http::response<http::string_body>& resp, boost::asio::yield_context yield,
    bs::error_code& ec, const std::chrono::milliseconds& timeout = std::chrono::milliseconds::zero());
private:
  std::shared_ptr<http_server_connection> shared_from_this();
};

class http_client : public async_client<async_tcp_handler, base_client_config>
{
public:
  http_client(boost::asio::io_context& ioc, std::unique_ptr<base_client_config>&& config);
protected:
  http::response<http::string_body> recv(boost::asio::yield_context yield,
    bs::error_code& ec, const std::chrono::milliseconds& timeout = std::chrono::milliseconds::zero());
  void send(const http::request<http::string_body>& req, boost::asio::yield_context yield,
    bs::error_code& ec, const std::chrono::milliseconds& timeout = std::chrono::milliseconds::zero());
private:
  std::shared_ptr<http_client> shared_from_this();
};