﻿#include "http_base.h"

#pragma region http_server_connection
http_server_connection::http_server_connection(tcp::socket&& s) :
  async_tcp_handler{ std::move(s) } {}
http::request<http::string_body> http_server_connection::recv(boost::asio::yield_context yield, bs::error_code& ec, const std::chrono::milliseconds& timeout)
{
  return receive_message<boost::beast::tcp_stream, true>(_tcp_stream, yield, ec, timeout);
}
void http_server_connection::send(const http::response<http::string_body>& resp, boost::asio::yield_context yield, bs::error_code& ec, const std::chrono::milliseconds& timeout)
{
  send_message<boost::beast::tcp_stream, false>(_tcp_stream, resp, yield, ec, timeout);
}
std::shared_ptr<http_server_connection> http_server_connection::shared_from_this()
{
  return std::static_pointer_cast<http_server_connection>(async_tcp_handler::shared_from_this());
}
#pragma endregion http_server_connection

#pragma region http_client
http_client::http_client(boost::asio::io_context& ioc, std::unique_ptr<base_client_config>&& config) :
  async_client(ioc, std::move(config)) {}
http::response<http::string_body> http_client::recv(boost::asio::yield_context yield, bs::error_code& ec, const std::chrono::milliseconds& timeout)
{
  return receive_message<boost::beast::tcp_stream, false>(_tcp_stream, yield, ec, timeout);
}
void http_client::send(const http::request<http::string_body>& req, boost::asio::yield_context yield, bs::error_code& ec, const std::chrono::milliseconds& timeout)
{
  send_message<boost::beast::tcp_stream, true>(_tcp_stream, req, yield, ec, timeout);
}
std::shared_ptr<http_client> http_client::shared_from_this()
{
  return std::static_pointer_cast<http_client>(async_tcp_handler::shared_from_this());
}
#pragma endregion http_client