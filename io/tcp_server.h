﻿#pragma once
#include <mutex>

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/asio.hpp>

#include "config.h"
#include "log.h"
#include "async_tcp_handler.h"

#define OPTIMAL_MAX_CONNECTION_SIZE 1000

namespace ba = boost::asio;
namespace bs = boost::system;
using tcp = boost::asio::ip::tcp;

class connection_list
{
public:
  connection_list(size_t optimal_capacity); // После набора optimal_capacity начинаем самоочищаться
  std::shared_ptr<async_tcp_handler> append(const std::shared_ptr<async_tcp_handler>& connection);
  void foreach(const std::function<bool(std::shared_ptr<async_tcp_handler>&)>& action);
  void foreach(const std::function<bool(const std::shared_ptr<const async_tcp_handler>&)>& action) const;
  size_t size() const;
private:
  void clean(); // Затираем закрытые соединения
private:
  const size_t _optimal_capacity;
  std::list<std::shared_ptr<async_tcp_handler>> _connections;
  mutable std::mutex _mutex;
};

class base_tcp_server
{
public:
  base_tcp_server(boost::asio::io_context& ioc, std::unique_ptr<base_server_config>&& config);
  virtual bool start();
  void stop();
  bool restart();
  std::vector<std::pair<std::string, connection_status>> get_connections_status() const;
  void disconnect_all();
  std::shared_ptr<async_tcp_handler> lookup(std::string_view client_ip_address);
protected:
  virtual std::shared_ptr<async_tcp_handler> make_connection(tcp::socket&& s) = 0;
private:
  void listening(boost::asio::yield_context yield);  
protected:
  std::unique_ptr<base_server_config> _config;
private:
  tcp::acceptor _acceptor;
  connection_list _connections;
};
