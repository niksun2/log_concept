#include "async_tcp_handler.h"
#include "config.h"

template<class async_protocol_handler, typename client_config>
class async_client: public async_protocol_handler
{
public:
  async_client(boost::asio::io_context& ioc, std::unique_ptr<client_config>&& config);
  bool connect();
private:
  void on_connect(bs::error_code ec);
  std::shared_ptr<async_client<async_protocol_handler, client_config>> shared_from_this();
protected:
  std::unique_ptr<client_config> _config;
};