#include "init.h"

void init_boost_log(boost::log::trivial::severity_level level, std::string const& filename)
{
  logging::core::get()->set_filter(logging::trivial::severity >= level);
  logging::core::get()->set_logging_enabled(true);
  logging::core::get()->add_global_attribute("TimeStamp", logging::attributes::local_clock());
  auto file_sink = boost::make_shared<logging::sinks::synchronous_sink<logging::sinks::text_file_backend>>(
    logging::keywords::open_mode = (std::ios::out),
    logging::keywords::file_name = filename + "_%Y-%m-%d_%5N.log",
    logging::keywords::auto_flush = true,
    logging::keywords::rotation_size = 5 * 1024 * 1024, // 5 Mb
    logging::keywords::time_based_rotation = logging::sinks::file::rotation_at_time_point(0, 0, 0)
    );
  file_sink->set_formatter(
    logging::expressions::stream << "["
    << logging::expressions::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S")
    << "] <" << logging::trivial::severity << "> " << logging::expressions::message
  );
  logging::core::get()->remove_all_sinks();
  logging::core::get()->add_sink(file_sink);
}