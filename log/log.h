#pragma once
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>

#ifdef _MSC_VER
  #define __PRETTY_FUNCTION__ __FUNCSIG__
#endif
#define LOG( lvl, nm, expr ) \
  do { \
      BOOST_LOG_TRIVIAL( lvl ) \
        << "\n  Function: " << __PRETTY_FUNCTION__ << "::line " << __LINE__ << "\n  Message: "\
        << nm \
        << ": " \
        expr; \
  } while (false)

#define LOG_STUB \
  do {} while (false)


#if defined(LOG)
#define TLOG( nm, expr ) LOG( trace, nm, expr )
#define DLOG( nm, expr ) LOG( debug, nm, expr )
#define ILOG( nm, expr ) LOG( info, nm, expr )
#define WLOG( nm, expr ) LOG( warning, nm, expr )
#define ELOG( nm, expr ) LOG( error, nm, expr   )
#define FLOG( nm, expr ) LOG( fatal, nm, expr )
#else
#define TLOG( nm, expr ) LOG_STUB
#define ILOG( nm, expr ) LOG_STUB
#define ILOG( nm, expr ) LOG_STUB
#define WLOG( nm, expr ) LOG_STUB
#define ELOG( nm, expr ) LOG_STUB
#define FLOG( nm, expr ) LOG_STUB
#endif

#define EXPAND_EC(ec) ec.value() << " - " << ec.message()
