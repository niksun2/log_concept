﻿#pragma once
#include <boost/beast/http.hpp>

namespace http = boost::beast::http;

http::response<http::string_body> make_default_response(std::string_view body)
{
  http::response<http::string_body> response;
  response.result(http::status::ok);
  response.version(11);
  response.set(http::field::server, "Beast");
  response.body() = body;
  response.content_length(response.body().size());
  return response;
}

http::request<http::string_body> make_default_request(std::string const& url, std::string_view body = "")
{
  http::request<http::string_body> request;
  request.version(11);
  request.target(url);
  if (!body.empty())
  {
    request.method(http::verb::post);
    request.body() = body;
    request.content_length(request.body().size());
  }
  else
    request.method(http::verb::get);
  return request;
}